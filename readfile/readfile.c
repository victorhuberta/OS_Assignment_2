/**
 * Module: readfile
 * Author: Victor Huberta
 * ```
 * Responsible for reading lines from input file
 * and write them onto the lt_buf
 * (shared buffer of lines).
 *
 * Mutexes used:
 * - inf_mtx -> for reading from input file.
 * - lt_buf_w_mtx -> for writing onto the lt_buf.
 *
 * - check_taken_mtx -> for checking if a line
 *   has been taken (read and removed by a writer thread).
 *
 * - not_taken_cond -> a condition mutex for blocking if
 *   the line has not been taken.
 *
 * All mutexes are declared in the main module.
 */

#include "readfile.h"

void _read_insert_lines(FILE *p_in, char *lm_buf);
void _unblock_writer_threads(void);
void _insert_line(char *lm_buf);
void _set_item_on_null(char *la_buf);

/**
 * Load lines from input file to lt_buf (shared buffer).
 *
 * Algorithm:
 * 1. Read a line from input file.
 * 2. Check if the current space in lt_buf is empty.
 * 3. If it is, insert the new line.
 *    Else, block until the writer threads take the
 *    line occupying it, then unblock and insert
 *    the new line.
 * 4. After the thread has done (meets EOF), set
 *    the global EOF flag.
 * 5. For every iteration, unblock all writer threads.
 */
void *load_lines(void *p_in)
{
    char lm_buf[N_MAX_BUF] = {0};

    _read_insert_lines((FILE *) p_in, lm_buf);

    eof_flag = 1;
    _unblock_writer_threads();

    return NULL;
}

/**
 * Read every line from input file, store it
 * in lm_buf (maximum line buffer), and insert
 * it into lt_buf.
 */
void _read_insert_lines(FILE *p_in, char *lm_buf)
{
    while (1) {
        pthread_mutex_lock(&inf_mtx);
        #ifdef DEBUG
            print_lt_buf("reader");
        #endif

        if (fgets(lm_buf, N_MAX_BUF, p_in) == NULL) {
            pthread_mutex_unlock(&inf_mtx);
            return;
        }

        pthread_mutex_lock(&lt_buf_w_mtx);
        pthread_mutex_unlock(&inf_mtx);

        _insert_line(lm_buf);

        pthread_mutex_unlock(&lt_buf_w_mtx);
        _unblock_writer_threads();
    }
}

void _unblock_writer_threads(void)
{
    pthread_mutex_lock(&check_eof_mtx);
    pthread_cond_broadcast(&not_eof_cond);
    pthread_mutex_unlock(&check_eof_mtx);
}

/**
 * Copy string from lm_buf (maximum line buffer)
 * to la_buf (actual line buffer), and insert
 * la_buf to lt_buf.
 */
void _insert_line(char *lm_buf)
{
    char *la_buf;
    int buf_size = strlen(lm_buf) + 1;

    la_buf = calloc(buf_size, sizeof *la_buf);
    ptr_exit_on_err("calloc", la_buf);

    strncpy(la_buf, lm_buf, buf_size);

    _set_item_on_null(la_buf);
}

/**
 * Insert new line only after the occupying
 * line has been taken.
 */
void _set_item_on_null(char *la_buf)
{
    pthread_mutex_lock(&check_taken_mtx);

    while (lt_buf[write_i] != NULL)
        pthread_cond_wait(&not_taken_cond, &check_taken_mtx);
    lt_buf[write_i++] = la_buf;
    RESET_INDEX(write_i);

    pthread_mutex_unlock(&check_taken_mtx);
}
