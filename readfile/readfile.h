#ifndef READFILE_H
#define READFILE_H

#include "../os_assignment_2.h"
#include "../utils/errors.h"
#ifdef DEBUG
    #include "../utils/debug.h"
#endif

#define N_MAX_BUF 1024 /* Includes newline and terminating null byte */

void *load_lines(void *p_in);

#endif
