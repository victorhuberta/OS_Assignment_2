#ifndef ERRORS_H
#define ERRORS_H

#include "../os_assignment_2.h"
#include "../args/getargs.h"

#define ENOTNUM -2
#define EBADSTR -3

#define NOT(fact) (! (fact))

void print_usage_exit(char *prog);

void errno_exit_on_err(char *func_name, int res);
void ptr_exit_on_err(char *func_name, void *res);
void std_exit_on_err(char *func_name, int res);
void exit_on_err(char *func_name, int res, int err_sign);
void perror_exit(char *str);

#endif
