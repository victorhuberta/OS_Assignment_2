/**
 * Module: errors
 * Author: Victor Huberta
 * ```
 * Responsible for providing better error
 * handling functions.
 */

#include "errors.h"

void print_usage_exit(char *prog)
{
    fprintf(stderr, "Usage: %s -i input_file -o output_file "
        "[-n thread_num (%d-%d)]\n", prog, N_MIN_THREADS, N_MAX_THREADS);
    exit(EXIT_FAILURE);
}

void errno_exit_on_err(char *func_name, int res)
{
    if (res != 0 && errno != 0) perror_exit(func_name);
}

void ptr_exit_on_err(char *func_name, void *res)
{
    if (res == NULL) perror_exit(func_name);
}

void std_exit_on_err(char *func_name, int res)
{
    exit_on_err(func_name, res, -1);
}

void exit_on_err(char *func_name, int res, int err_sign)
{
    if (res == err_sign) perror_exit(func_name);
}

void perror_exit(char *str)
{
    perror(str);
    exit(EXIT_FAILURE);
}
