/**
 * Module: conversions
 * Author: Victor Huberta
 * ```
 * Responsible for converting values from
 * a type to another.
 */

#include "conversions.h"

/**
 * Convert a string to a long integer.
 */
long to_long(char *str)
{
    char *endptr;
    long val;

    if (str == NULL || *str == '\0') {
        errno = EBADSTR;
        return -1;
    }

    val = strtol(str, &endptr, 10);

    if (*endptr != '\0') {
        errno = ENOTNUM;
        return -1;
    }

    return val;
}
