/**
 * Module: os_assignment_2
 * Author: Victor Huberta
 * ```
 * Responsible for starting as many threads as
 * the user wants, which would read all the lines
 * from the input file and copy them to the output
 * file.
 *
 * NOTE: Please read the README for more details.
 */

#include "os_assignment_2.h"
#include "args/getargs.h"
#include "utils/errors.h"
#include "utils/conversions.h"
#include "readfile/readfile.h"
#include "writefile/writefile.h"

void _start_and_wait_all_threads(long t_num, FILE *p_in, FILE *p_out);
pthread_t *_start_read_threads(long t_num, FILE *p_in);
pthread_t *_start_write_threads(long t_num, FILE *p_out);
void _wait_for_all_threads(long t_num,
    pthread_t *read_threads, pthread_t *write_threads);

/**
 * All the mutexes are separated to keep the critical section
 * small, and so CPU resources usage can be optimized.
 * Mutexes' definitions can be found in the header file.
 */
pthread_mutex_t inf_mtx = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t outf_mtx = PTHREAD_MUTEX_INITIALIZER;

pthread_mutex_t lt_buf_r_mtx = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t lt_buf_w_mtx = PTHREAD_MUTEX_INITIALIZER;

pthread_mutex_t check_taken_mtx = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t not_taken_cond = PTHREAD_COND_INITIALIZER;

pthread_mutex_t check_eof_mtx = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t not_eof_cond = PTHREAD_COND_INITIALIZER;

int eof_flag = 0;
int read_i = 0;
int write_i = 0;

/**
 * Get user's arguments, start all threads,
 * wait for them to finish, then close all files
 * and free the shared buffer memory.
 */
int main(int argc, char **argv)
{
    FILE *p_in = NULL, *p_out = NULL;
    long t_num;

    lt_buf = calloc(N_MAX_LINES, sizeof *lt_buf);
    ptr_exit_on_err("calloc", lt_buf);

    puts("Getting arguments...");
    init_args(argc, argv);
    getargs(&p_in, &p_out, &t_num);

    _start_and_wait_all_threads(t_num, p_in, p_out);

    puts("Done.");

    errno_exit_on_err("fclose", fclose(p_in));
    errno_exit_on_err("fclose", fclose(p_out));
    free(lt_buf);

    exit(EXIT_SUCCESS);
}

/**
 * Start threads and store their IDs in arrays.
 * Then wait for them to finish their jobs.
 */
void _start_and_wait_all_threads(long t_num, FILE *p_in, FILE *p_out)
{
    pthread_t *read_threads, *write_threads;

    puts("Starting reader threads and writer threads...");
    read_threads = _start_read_threads(t_num, p_in);
    write_threads = _start_write_threads(t_num, p_out);

    _wait_for_all_threads(t_num, read_threads, write_threads);

    free(read_threads);
    free(write_threads);
}

pthread_t *_start_read_threads(long t_num, FILE *p_in)
{
    int i;
    pthread_t *read_threads = calloc(t_num, sizeof *read_threads);
    ptr_exit_on_err("calloc", read_threads);

    for (i = 0; i < t_num; ++i)
        pthread_create(&read_threads[i], NULL, load_lines, p_in);

    return read_threads;
}

pthread_t *_start_write_threads(long t_num, FILE *p_out)
{
    int i;
    pthread_t *write_threads = calloc(t_num, sizeof *write_threads);
    ptr_exit_on_err("calloc", write_threads);

    for (i = 0; i < t_num; ++i)
        pthread_create(&write_threads[i], NULL, save_lines, p_out);

    return write_threads;
}

void _wait_for_all_threads(long t_num,
    pthread_t *read_threads, pthread_t *write_threads)
{
    int i;

    puts("Waiting for them to finish...");
    for (i = 0; i < t_num; ++i)
        pthread_join(read_threads[i], NULL);

    for (i = 0; i < t_num; ++i)
        pthread_join(write_threads[i], NULL);
}
