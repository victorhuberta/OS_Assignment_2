CC=gcc
CFLAGS=-ansi -pedantic -Wall
LIBS=-pthread

C_FILES=utils/errors.c utils/conversions.c args/getargs.c readfile/readfile.c \
writefile/writefile.c os_assignment_2.c
DBG_C_FILE=utils/debug.c
EXECUTABLE= os_assignment_2

TEST_INPUT=tests/input_file
TEST_OUTPUT=tests/output_file

DBG_TEST_INPUT=tests/dbg_input_file
DBG_TEST_OUTPUT=tests/dbg_output_file

all: build_test

build_test: build test

build:
	$(CC) $(CFLAGS) $(LIBS) $(C_FILES) -o $(EXECUTABLE)

test:
	./$(EXECUTABLE) -i $(TEST_INPUT) -o $(TEST_OUTPUT) -n 3
	diff $(TEST_INPUT) $(TEST_OUTPUT)

debug_test: debug test_dbg

debug:
	$(CC) $(CFLAGS) $(LIBS) -DDEBUG $(C_FILES) $(DBG_C_FILE) -o $(EXECUTABLE)

test_dbg:
	./$(EXECUTABLE) -i $(DBG_TEST_INPUT) -o $(DBG_TEST_OUTPUT) -n 3
	diff $(DBG_TEST_INPUT) $(DBG_TEST_OUTPUT)

memcheck: build
	valgrind --leak-check=yes \
	./$(EXECUTABLE) -i $(TEST_INPUT) -o $(TEST_OUTPUT) -n 3

clean:
	rm -rf $(EXECUTABLE) $(TEST_OUTPUT) $(DBG_TEST_OUTPUT)
