/**
 * Module: getargs
 * Author: Victor Huberta
 * ```
 * Responsible for getting all program arguments
 * from the user and return them to the main module.
 */

#include "getargs.h"

void _assign_p_in(FILE **p_in);
void _assign_p_out(FILE **p_out);
void _assign_t_num(long *pt_num);
void _check_args_validity(FILE **p_in, FILE **p_out, long *pt_num);
void _pinval_t_num(void);

/* args_read: counter of read arguments. */
static int argc, args_read = 0;
static char **argv;

/**
 * Receive the number of arguments and
 * their values from the main module.
 */
void init_args(int count, char **args)
{
    argc = count;
    argv = args;
}

/**
 * Store arguments into their respective
 * variables. Check their validity before returning.
 */
void getargs(FILE **p_in, FILE **p_out, long *pt_num)
{
    int opt;

    while ((opt = getopt(argc, argv, "i:o:n:")) != -1) {
        switch (opt) {
            case 'i':
                _assign_p_in(p_in);
                break;
            case 'o':
                _assign_p_out(p_out);
                break;
            case 'n':
                _assign_t_num(pt_num);
                break;
            default:
                print_usage_exit(argv[0]);
        }
    }

    _check_args_validity(p_in, p_out, pt_num);
}

/**
 * Open the input file and assign it to p_in.
 */
void _assign_p_in(FILE **p_in)
{
    if (*p_in != NULL) return;

    *p_in = fopen(optarg, "r");
    ptr_exit_on_err("fopen", *p_in);
    ++args_read;
}

/**
 * Open the output file and assign it to p_out.
 */
void _assign_p_out(FILE **p_out)
{
    if (*p_out != NULL) return;

    *p_out = fopen(optarg, "w");
    ptr_exit_on_err("fopen", *p_out);
    ++args_read;
}

/**
 * Convert argument to long integer and assign
 * it to t_num. Use N_DEFAULT_THREADS if
 * number isn't valid.
 */
void _assign_t_num(long *pt_num)
{
    *pt_num = to_long(optarg);

    /* On error set t_num to its default. */
    if (*pt_num == -1 && errno != 0) {
        _pinval_t_num();
        *pt_num = (long) N_DEFAULT_THREADS;
    }
}

/**
 * Check whether or not everything is fine.
 * Also check if t_num's value is out of the given range.
 */
void _check_args_validity(FILE **p_in, FILE **p_out, long *pt_num)
{
    if ((args_read < N_REQUIRED_ARGS) || (*p_in == NULL) || (*p_out == NULL))
        print_usage_exit(argv[0]);

    if (NOT(IN_RANGE(*pt_num, N_MIN_THREADS, N_MAX_THREADS))) {
        _pinval_t_num();
        *pt_num = (long) N_DEFAULT_THREADS;
    }
}

void _pinval_t_num(void)
{
    fprintf(stderr,
        "Invalid or no thread number was given, "
        "using the default number: %d\n", N_DEFAULT_THREADS);
}
